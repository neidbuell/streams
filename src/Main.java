import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;

public class Main {

    public static void main(String[] args) {
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario", "Milan");
        Trader alan = new Trader("Alan", "Cambridge");
        Trader brian = new Trader("Brian", "Cambridge");

        List<Transaction> transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );

/*
        The questions
        1 Find all transactions in the year 2011 and sort them by value (small to high).
        2 What are all the unique cities where the traders work?
        3 Find all traders from Cambridge and sort them by name.
        4 Return a string of all traders’ names sorted alphabetically.
        5 Are any traders based in Milan?
        6 Print all transactions’ values from the traders living in Cambridge.
        7 What’s the highest value of all the transactions?
        8 Find the transaction with the smallest value.*/

        transactions.stream()
                .filter(transaction -> transaction.getYear() == 2011)
                .sorted(comparing(Transaction::getValue))
                .forEach(System.out::println);
        System.out.println("1) -----------");

        List<String> cities =  transactions.stream()
                .map(Transaction::getTrader)
                .map(Trader::getCity)
                .distinct()
                .collect(Collectors.toList());

        cities.stream().forEach(System.out::println);
        System.out.println("2) -----------");

        List<String> tradersFromCambridge = transactions.stream()
                .map(Transaction::getTrader)
                .filter(trader -> trader.getCity().equals("Cambridge"))
                .map(Trader::getName)
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        tradersFromCambridge.stream()
                .forEach(System.out::println);
        System.out.println("3) -----------");


        String traders = transactions.stream()
                .map(Transaction::getTrader)
                .map(Trader::getName)
                .distinct()
                .sorted(String::compareTo)
                .reduce("", (n1, n2) -> n1 + " " + n2);
        System.out.println(traders);
        System.out.println("4) -----------");


        Boolean traderInMilan = transactions.stream()
                .map(Transaction::getTrader)
                .filter(t -> t.getCity().equalsIgnoreCase("Milan"))
                .findAny()
                .isPresent();

        System.out.println(traderInMilan);
        System.out.println("5) -----------");

        transactions.stream()
                .forEach(t -> {if (t.getTrader().getCity().equalsIgnoreCase("Cambridge")) {
                System.out.println(t.getTrader().getName() + ": " + t.getValue());}
                });

        System.out.println("6) -----------");

        Optional<Integer> highValue = transactions.stream()
                .map(Transaction::getValue)
                .reduce(Integer::max);
        System.out.println(highValue);
        System.out.println("7) -----------");

        Optional<Transaction> lowest = transactions.stream()
                .reduce((t1, t2) -> t1.getValue() < t2.getValue() ? t1 : t2);

        System.out.println(lowest);
        System.out.println("8a) -----------");

        OptionalInt lowest2 = transactions.stream()
                .mapToInt(Transaction::getValue)
                .min();
        System.out.println(lowest2.orElse(99));
        System.out.println("8b) -----------");

        IntStream evenNumbers = IntStream.rangeClosed(1,20)
                .filter(n -> n % 2 == 0);

        evenNumbers.forEach(System.out::println);
        System.out.println("9) -------------");

        Stream<String> stream = Stream.of("Bob", "Stan", "Chris", "Charles");
        stream.map(String::toUpperCase)
                .forEach(System.out::println);
        System.out.println("10) -------------");

        int[] numbers = {2,4,6,10};
        int sum = Arrays.stream(numbers)
                .sum();
        System.out.println(sum);
        System.out.println("11) -------------");
    }
}

